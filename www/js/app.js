angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {


    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      StatusBar.styleLightContent();
    }
  });
})

/*

ROMAQUE AJUDA AQUI :D
ao clicar ir para o html correspondente, mas o mesmo rodapé a cabeçalho para todas as telas
*/

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

  $stateProvider

  // Aqui ea directive da tabs
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'views/tabs.html'
  })

  // Each tab has its own nav history stack:

  .state('tab.registrar', {
    // Aqui e a URl da pagina
    url: '/vereador',
    views: {
      'tab-registrar': {
        // Aqui eo template da Pagina
        templateUrl: 'views/pages/vereador.html',
        // Aqui vc tambem pode colocar um controller ex:
        // controller: "vereadorCtrl"
      }
    }
  })

  .state('tab.meusregistros', {
      url: '/meus-registros',
      views: {
        'tab-meus-registros': {
          templateUrl: 'views/pages/meus-registros.html'
        }
      }
    })
    .state('tab.meuperfil', {
      url: '/meu-perfil',
      views: {
        'tab-meu-perfil': {
          templateUrl: 'views/pages/meu-perfil.html'
        }
      }
    });

  // Aqui e a pagina 404
  $urlRouterProvider.otherwise('/tab/vereador');

  // Aqui e a posição da tabs se mudar pra bottom ele fica no rodape
  $ionicConfigProvider.tabs.position('top');

});
